# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'a98325ebab0ce6156d1b4da4aeb4979d4b87cddaa74c76edf793874ca58be4dac1ba25a42f5c7c7f8db57769a4206465d7a3e0d04a892d867930b11c126f693a'
